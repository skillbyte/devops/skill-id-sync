from dataclasses import dataclass


@dataclass
class User:
    name: str
    email: str
    active: bool

    @classmethod
    def from_o365_user(cls, user) -> "User":
        return cls(user.display_name, user.user_principal_name, user.account_enabled)

    @classmethod
    def from_cloud_identity_user(cls, user: dict) -> "User":
        return cls(
            user["name"]["fullName"], user["primaryEmail"], not user["suspended"]
        )
