import os
import string
import secrets
import O365

from common import User


def generate_strong_password(length=64, include_special_chars=True):
    characters = string.ascii_letters + string.digits
    if include_special_chars:
        characters += string.punctuation

    password = "".join(secrets.choice(characters) for i in range(length))
    return password


class NewUser:
    def __init__(self, displayname, email):
        self.displayname = displayname
        self.email = email
        self.nickname = email[: email.find("@")]
        self.password = generate_strong_password()
        self.data = {
            "accountEnabled": True,
            "displayName": self.displayname,
            "mailNickname": self.nickname,
            "userPrincipalName": self.email,
            "passwordProfile": {
                "forceChangePasswordNextSignIn": False,
                "password": self.password,
            },
            "onPremisesImmutableId": self.email
        }


class Entra:
    def __init__(self):
        credentials = (os.environ["ARM_CLIENT_ID"], os.environ["ARM_CLIENT_SECRET"])
        self.account = O365.Account(
            credentials,
            auth_flow_type="credentials",
            tenant_id=os.environ["ARM_TENANT_ID"],
        )
        self.__get_users()

    def create_user(self, displayname, email):
        if email in self.users.keys():
            pass
        new_user = NewUser(displayname, email)
        connection = self.account.connection.get_session(
            scopes=["User.ReadWrite.All"], load_token=True
        )
        response = connection.post(
            "https://graph.microsoft.com/v1.0/users", json=new_user.data
        )
        if response.status_code == 201:
            print("User created successfully.")
        else:
            print(f"Failed to create user: {response.json()}")
        return response

    def __get_users(self):
        directory = self.account.directory()
        query = directory.q().select(
            "displayName", "userPrincipalName", "mail", "accountEnabled"
        )
        self.users = {
            user.user_principal_name: User.from_o365_user(user)
            for user in directory.get_users(query=query)
        }

    def update_user(self, email: str, active: bool):
        if email not in self.users.keys():
            raise ValueError("Unknown user")
        if self.users[email].active != active:
            connection = self.account.connection.get_session(
                scopes=["User.ReadWrite.All"], load_token=True
            )
            response = connection.patch(
                f"https://graph.microsoft.com/v1.0/users/{email}",
                json={"accountEnabled": active},
            )
            if response.status_code == 204:
                print("User de-/reactivated successfully.")
            else:
                print(f"Failed to de-/reactivate user: {response.json()}")
            return response
