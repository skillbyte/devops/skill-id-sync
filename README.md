# `skill-id-sync`

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Sync Google Cloud Identity user to Entra ID

This Python script reads users from Cloud Identity and creates them in Entra ID if they don't exist already. If users
are deleted or suspended in Cloud Identity, they will be disabled in Entra ID. If a user gets unsuspended they will also
be reenabled in Entra ID. Full deletion and recreation of users is not supported.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

There's no installation process as such, but you can create a virtualenv and install the required packages in it like
so:

```sh
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Usage

You'll need to acquire credentials for both Cloud Identity and Entra ID. For Cloud Identity that means creating a
`credentials.json` and for Azure that means filling and sourcing the environment in the `example.env` (copy it to `.env`
first though)

For Cloud Identity you can refer to
[this example from the Google documentation](https://developers.google.com/admin-sdk/directory/v1/quickstart/python)
since `skill-id-sync` uses the same authentication mechanism.

For Entra ID you'll need to create an App Registration, give it the MS Graph `User.ReadWrite.All` permissions (don't
forget granting admin consent), create an Application Secret and note down the client id, client secret and tenant id in
your `.env` file.

```sh
source .env
python skill-id-sync.py
```

## Maintainers

[@kfkonrad](https://gitlab.com/kfkonrad)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the
[standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2023 Kevin F. Konrad
