from entra import Entra
from cloud_identity import CloudIdentity

entra = Entra()
cloud_identity = CloudIdentity()

print("Create users (if not exists)\n")

users_not_in_entra = set(cloud_identity.users.keys()) - set(entra.users.keys())

for email in users_not_in_entra:
    name = cloud_identity.users[email].name
    print(f"create_user({name}, {email})")
    response = entra.create_user(name, email)
    print(response)


print("\nSync active status from Cloud Identity to Entra ID\n")

users_in_both_systems = set(cloud_identity.users.keys()) & set(entra.users.keys())

for email in users_in_both_systems:
    ci_user = cloud_identity.users[email]
    entra_user = entra.users[email]
    if ci_user.active != entra_user.active:
        print(f"entra.update_user({email}, active={ci_user.active})")
        response = entra.update_user(email, active=ci_user.active)
        print(response)


print("\nDeactivate users in EntraID if not found in Cloud Identity\n")

users_not_in_cloud_identity = set(entra.users.keys()) - set(cloud_identity.users.keys())

for email in users_not_in_cloud_identity:
    if ci_user.active != entra_user.active:
        print(f"entra.update_user({email}, active=False)")
        response = entra.update_user(email, active=False)
        print(response)
