import os.path

import json

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build

from common import User

SCOPES = ["https://www.googleapis.com/auth/admin.directory.user"]


class CloudIdentity:
    def __init__(self):
        self.credentials = self.__get_credentials()
        self.directory = build("admin", "directory_v1", credentials=self.credentials)
        self.__get_users()

    def __get_credentials(self):
        creds = None
        # The file token.json stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if (
            os.path.exists("token.json")
            and json.load(open("token.json"))["scopes"] == SCOPES
        ):
            try:
                with open("token.json") as token_file:
                    if json.load(token_file)["scopes"] == SCOPES:
                        creds = Credentials.from_authorized_user_file(
                            "token.json", SCOPES
                        )
            except Exception:
                pass
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    "credentials.json", SCOPES
                )
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open("token.json", "w") as token:
                token.write(creds.to_json())
        return creds

    def __get_users(self):
        raw_users = (
            self.directory.users()
            .list(domain="skillbyte.de", maxResults=100, orderBy="email")
            .execute()
            .get("users", [])
        )
        self.users = {
            elem["primaryEmail"]: User.from_cloud_identity_user(elem)
            for elem in raw_users
        }
